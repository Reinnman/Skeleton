# Skeleton
[![License](https://img.shields.io/badge/License-MIT-2196F3)](https://opensource.org/license/mit)
[![Akimeite](https://img.shields.io/badge/%E3%82%84%E3%81%8C%E3%81%BF%E3%81%82%E3%81%8D-Skeleton-FFEB3B)](https://gitee.com/Akimeite/Skeleton)
[![JitPack](https://img.shields.io/badge/JitPack-1.0.30-4CAF50)](https://jitpack.io/#com.gitee.Akimeite/Skeleton)
[![MinSdk](https://img.shields.io/badge/MinSdk-23%2B-FF9800)](https://developer.android.com/tools/releases/platforms#6.0)
## Usage
### settings.gradle
```gradle
dependencyResolutionManagement {
	...
	repositories {
		...
		mavenCentral()
		maven { url "https://jitpack.io" }
	}
}
```
### build.gradle
```gradle
implementation 'com.gitee.Akimeite:Skeleton:$latestVersion'
```
