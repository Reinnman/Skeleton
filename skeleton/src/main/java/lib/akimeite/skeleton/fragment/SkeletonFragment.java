package lib.akimeite.skeleton.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewbinding.ViewBinding;

import com.dylanc.viewbinding.base.ViewBindingUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Yagami Aki on 2022/01/18 10:36 with Android Studio.
 */
public abstract class SkeletonFragment<VB extends ViewBinding> extends Fragment {

	protected VB mBinding;
	protected FragmentActivity mActivity;
	protected Context mContext;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		mBinding = ViewBindingUtil.inflateWithGeneric(this, inflater, container, false);
		mActivity = getActivity();
		mContext = getContext();
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
		return mBinding.getRoot();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mBinding = null;
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
	}

	@Subscribe(threadMode = ThreadMode.BACKGROUND)
	public void onSkeletonFragmentEvent(Void v) {}
}
