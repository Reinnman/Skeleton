package lib.akimeite.skeleton.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.dylanc.viewbinding.base.ViewBindingUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Yagami Aki on 2022/01/18 10:28 with Android Studio.
 */
public abstract class SkeletonActivity<VB extends ViewBinding> extends AppCompatActivity {

	protected VB mBinding;
	protected AppCompatActivity mActivity;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
		mActivity = this;
		mBinding = ViewBindingUtil.inflateWithGeneric(this, getLayoutInflater());
		if (!addView()) {
			setContentView(mBinding.getRoot());
		}
	}

	@Override
	protected void onDestroy() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		super.onDestroy();
	}

	protected boolean addView() {
		return false;
	}

	@Subscribe(threadMode = ThreadMode.BACKGROUND)
	public void onSkeletonActivityEvent(Void v) {}
}
