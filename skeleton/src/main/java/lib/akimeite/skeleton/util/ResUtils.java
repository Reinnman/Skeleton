package lib.akimeite.skeleton.util;

import androidx.annotation.BoolRes;
import androidx.annotation.IntegerRes;

import com.blankj.utilcode.util.Utils;

/**
 * Created by Yagami Aki on 2022/01/18 15:05 with Android Studio.
 */
public class ResUtils {

	public static boolean getBoolean(@BoolRes int resId) {
		return Utils.getApp().getResources().getBoolean(resId);
	}

	public static int getInteger(@IntegerRes int resId) {
		return Utils.getApp().getResources().getInteger(resId);
	}
}
