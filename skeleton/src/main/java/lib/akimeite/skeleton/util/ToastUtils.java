package lib.akimeite.skeleton.util;

import androidx.annotation.StringRes;

/**
 * Created by Yagami Aki on 2022/01/18 15:04 with Android Studio.
 */
public class ToastUtils {

	public static void showLightShort(CharSequence text) {
		getLightToastUtils(false).show(text);
	}

	public static void showLightShort(@StringRes int resId) {
		getLightToastUtils(false).show(resId);
	}

	public static void showLightLong(CharSequence text) {
		getLightToastUtils(true).show(text);
	}

	public static void showLightLong(@StringRes int resId) {
		getLightToastUtils(true).show(resId);
	}

	public static void showDarkShort(CharSequence text) {
		getDarkToastUtils(false).show(text);
	}

	public static void showDarkShort(@StringRes int resId) {
		getDarkToastUtils(false).show(resId);
	}

	public static void showDarkLong(CharSequence text) {
		getDarkToastUtils(true).show(text);
	}

	public static void showDarkLong(@StringRes int resId) {
		getDarkToastUtils(true).show(resId);
	}

	public static com.blankj.utilcode.util.ToastUtils getLightToastUtils(boolean isLong) {
		return com.blankj.utilcode.util.ToastUtils.make()
		                                          .setMode(com.blankj.utilcode.util.ToastUtils.MODE.LIGHT)
		                                          .setDurationIsLong(isLong)
		                                          .setNotUseSystemToast();
	}

	public static com.blankj.utilcode.util.ToastUtils getDarkToastUtils(boolean isLong) {
		return com.blankj.utilcode.util.ToastUtils.make()
		                                          .setMode(com.blankj.utilcode.util.ToastUtils.MODE.DARK)
		                                          .setDurationIsLong(isLong)
		                                          .setNotUseSystemToast();
	}
}
