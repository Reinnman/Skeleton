package lib.akimeite.skeleton.task;

import com.blankj.utilcode.util.ThreadUtils;

/**
 * Created by Yagami Aki on 2022/01/24 09:38 with Android Studio.
 */
public abstract class SimpleMainTask extends ThreadUtils.SimpleTask<Void> {
	@Override
	public Void doInBackground() {
		return null;
	}

	@Override
	public void onSuccess(Void result) {
		onSuccess();
	}

	public abstract void onSuccess();
}
