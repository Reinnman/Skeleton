package lib.akimeite.skeleton.task;

import com.blankj.utilcode.util.ThreadUtils;

/**
 * Created by Yagami Aki on 2022/01/24 09:18 with Android Studio.
 */
public abstract class SimpleBackgroundTask extends ThreadUtils.SimpleTask<Void> {
	@Override
	public Void doInBackground() throws Throwable {
		onBackground();
		return null;
	}

	@Override
	public void onSuccess(Void result) {}

	public abstract void onBackground() throws Throwable;
}
