package lib.akimeite.skeleton.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by Yagami Aki on 2022/01/18 13:45 with Android Studio.
 */
public class MarqueeTextView extends AppCompatTextView {

	public MarqueeTextView(@NonNull Context context) {
		super(context);
		initView();
	}

	public MarqueeTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public MarqueeTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initView();
	}

	@Override
	public boolean isFocused() {
		return true;
	}

	private void initView() {
		setEllipsize(TextUtils.TruncateAt.MARQUEE);
		setSingleLine();
		setMarqueeRepeatLimit(-1);
	}
}
