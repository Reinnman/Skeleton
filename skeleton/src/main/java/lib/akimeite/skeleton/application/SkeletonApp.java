package lib.akimeite.skeleton.application;

import android.app.Application;

import com.blankj.utilcode.util.Utils;

/**
 * Created by Yagami Aki on 2022/01/18 10:43 with Android Studio.
 */
public class SkeletonApp extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Utils.init(this);
	}
}
