package lib.akimeite.skeleton.app.application;

import org.greenrobot.eventbus.EventBus;

import lib.akimeite.skeleton.SkeletonEventBusIndex;
import lib.akimeite.skeleton.application.SkeletonApp;

/**
 * Created by Yagami Aki on 2022/01/18 12:34 with Android Studio.
 */
public class App extends SkeletonApp {

	@Override
	public void onCreate() {
		super.onCreate();
		EventBus.builder().addIndex(new SkeletonEventBusIndex()).build();
	}
}
