package lib.akimeite.skeleton.app.activity;

import lib.akimeite.skeleton.activity.SkeletonActivity;
import lib.akimeite.skeleton.app.databinding.ActivityMainBinding;

public class MainActivity extends SkeletonActivity<ActivityMainBinding> {}